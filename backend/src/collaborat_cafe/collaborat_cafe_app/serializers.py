from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Questions, Answers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password')


class QuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questions
        fields = ('question_id', 'question', 'raised_by')


class AnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answers
        fields = ('answer_id', 'answer', 'upvote', 'answered_by', 'is_accepted', 'question_id')
