from django.urls import path

from . import views

urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('add_question/', views.add_question, name='add_question'),
    path('add_answer/', views.add_answer, name='add_answer'),
    path('get_questions/', views.get_questions, name='get_questions'),
    path('get_answers/', views.get_answers, name='get_answers'),
]
