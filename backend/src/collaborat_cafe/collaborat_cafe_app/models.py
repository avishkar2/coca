import uuid

from django.conf import settings
from django.db import models


class Questions(models.Model):
    question_id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    question = models.TextField()
    upvote = models.IntegerField(blank=True, null=True)
    raised_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='questions_raised_by',
        on_delete=models.PROTECT,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Answers(models.Model):
    answer_id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    answer = models.TextField()
    upvote = models.IntegerField(blank=True, null=True)
    answered_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='answer_raised_by',
        on_delete=models.PROTECT,
    )
    is_accepted = models.BooleanField(default=False)
    question_id = models.ForeignKey(
        Questions,
        related_name='question_details',
        on_delete=models.CASCADE,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
