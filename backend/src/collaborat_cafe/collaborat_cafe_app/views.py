import random, string

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from rest_framework.response import Response
from django.contrib.auth.models import User


from .serializers import UserSerializer, QuestionsSerializer, AnswersSerializer
from .models import Questions, Answers

def generate_password():
    return ''.join(random.choices(string.ascii_letters + string.digits, k=16))


@api_view(['POST'])
def register(request):
    if request.method == 'POST':
        data = {
            'email': request.data.get('email'),
            'username': request.data.get('email').split('@')[0],
            'password': generate_password(),
        }
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def login(request):
    if request.method == 'POST':
            email = request.data.get('email')
            password = request.data.get('password')
            try:
                User.objects.get(email=email, password=password)
                return Response({"message": "User is successfully loggedin"}, status=status.HTTP_200_OK)
            except User.DoesNotExist:
                return Response({"message": "User does not exist"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            except User.MultipleObjectsReturned:
                return Response({"message": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['POST'])
def add_question(request):
    if request.method == 'POST':
        try:
            user_id = User.objects.get(email=request.data.get('raised_by')).id
        except User.DoesNotExist:
            return Response({"message": "User does not exist"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except User.MultipleObjectsReturned:
            return Response({"message": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
        data = {
            'question': request.data.get('question'),
            'upvote': request.data.get('upvote', 0),
            'raised_by': user_id,
        }
        serializer = QuestionsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def add_answer(request):
    if request.method == 'POST':
        try:
            answered_by = User.objects.get(email=request.data.get('answered_by')).id
        except User.DoesNotExist:
            return Response({"message": "User does not exist"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except User.MultipleObjectsReturned:
            return Response({"message": "Something went wrong"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        data = {
            'answer': request.data.get('answer'),
            'upvote': request.data.get('upvote', 0),
            'is_accepted': request.data.get('is_accepted'),
            'question_id': request.data.get('question_id'),
            'answered_by': answered_by,
        }
        serializer = AnswersSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def get_questions(request):
    if request.method == 'GET':
        questions = list(Questions.objects.all())
        serializers = QuestionsSerializer(questions, many=True)
        return Response(serializers.data, status=status.HTTP_200_OK)
    return Response({"error": "Bad Request"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def get_answers(request):
    if request.method == 'POST':
        answers = list(Answers.objects.filter(question_id=request.data.get('question_id')))
        serializers = AnswersSerializer(answers, many=True)
        return Response(serializers.data, status=status.HTTP_200_OK)
    return Response({"error": "Bad Request"}, status=status.HTTP_400_BAD_REQUEST)
