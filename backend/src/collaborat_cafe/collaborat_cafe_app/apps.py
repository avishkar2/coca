from django.apps import AppConfig


class CollaboratCafeAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'collaborat_cafe_app'
