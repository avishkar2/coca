import * as React from 'react';
import PropTypes from 'prop-types';
import {
     AppBar,
     Box, 
     CssBaseline,
     List,ListItemText,
     Divider,
     Toolbar,
     ListItem,
     Drawer, 
     ListItemIcon,
     Button,
        Typography,
        MenuItem,
        IconButton
} from '@material-ui/core';
import { Link, useHistory } from "react-router-dom";
import {  Menu , ExitToApp} from '@material-ui/icons'
const drawerWidth = 240;

export default function TemporaryDrawer() {
    const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const history = useHistory();
    const [state, setState] = React.useState({
      top: false,
      left: false,
      bottom: false,
      right: false,
    });
    
    const handleLogout = () => {
        localStorage.setItem("status", false);
        history.push("/")
     };
  
    const AppbarCompo = () => {
        return (
            <AppBar position="static">
<Toolbar>
<IconButton aria-label="delete" style={{color:'white'}} onClick={toggleDrawer("left", true)}> 
  <Menu />
</IconButton>
<Typography variant="h6" style={{cursor: 'pointer'}} component="div" sx={{ flexGrow: 1 }}>
  Collaborate cafe
</Typography>
<Box style={{position:'relative' , flex:2}}>
<IconButton  aria-label="logout" style={{color:'white', float:'right'}} onClick={handleLogout}> 
  <ExitToApp style={{float:"right"}}/>
</IconButton>
</Box>
{auth && (
<div>

</div>
)}
</Toolbar>
</AppBar>
        )
    }

    const toggleDrawer = (anchor, open) => (event) => {
      if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
        return;
      }
        setState({ ...state, [anchor]: open });
    };
  
    const list = (anchor) => (
      <Box
        sx={{ width : 250 }}
        role="presentation"
        onClick={toggleDrawer(anchor, false)}
        onKeyDown={toggleDrawer(anchor, false)}
      >
        <List>
          {['Collaborate-Cafe'].map((text) => (
            <ListItem button key={text}>
              <ListItemIcon>
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          {[{name: 'Issues', link :'/home'}, {name: 'Sign up', link :'/home/createUser'}].map((text, index) => (
            <ListItem button key={index}>
              <ListItemIcon>
              </ListItemIcon>
              <Link to={text.link}><ListItemText primary={text.name} /></Link> 
            </ListItem>
          ))}
        </List>
      </Box>
    );
  
    return (
      <div>
            {AppbarCompo()}
            <Drawer
              anchor={"left"}
              open={state["left"]}
              onClose={toggleDrawer("left", false)}
            >
              {list("left")}
            </Drawer>

      </div>
    );
  }
