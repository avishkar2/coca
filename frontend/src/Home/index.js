import React, {useEffect, useState} from 'react';
import { useHistory,Route , Switch, Router } from "react-router-dom";
import ResponsiveDrawer from "./components/Drawer";

import { Button,
     Box,
      Typography,
       Modal,
        Grid,
         TextField, 
         Table, 
         TableBody,
          TableCell,
           TableRow , TableContainer,
            Paper,
            TableHead
        } from '@material-ui/core'
import { Menu } from '@material-ui/icons'
import SignUp from '../SignUp';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  const style2= {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };









export const FirstComponent = () => {
    const [open, setOpen] = useState(false);
    const [questionList, setQuestion] = useState([])
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [openAnswer, setAnswer] = useState(false);
    const [questionId, setQuetionId] = useState({});
    const [suggestion, setSuggesstion] = useState('');
    const [emailAnswer, setEmailAnswer] = useState('');

    const [answerList, setAnswerList] = useState([])

    const handleClose = () => {}
    
    const handleCloseAnswer = () => {

    }

    useEffect(()=> {
        if(!open){
            fetch("http://localhost:8080/get_questions/").then((data)=> data.json()).then((data)=> {
                console.log(data,'DATA')
                setQuestion([...data])
            }) 
        }
    },[open])

    const formSubmitAddQuestion = (e) => {
        e.preventDefault();
        const fetchMethod ={
            method: "POST",
            body: JSON.stringify({
                answered_by: emailAnswer,
                question_id: questionId.question_id,
                answer: suggestion,
                // upvotes:2,
                is_accepted: false
            }),
             // Adding headers to the request
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }
        fetch('http://localhost:8080/add_answer/',fetchMethod).then((data)=>data.json()).then((data)=>{
            console.log(data,'[SAMLPE]')
            setAnswer(false);
            setEmailAnswer('');
            setSuggesstion('')
        })
    }



    const answerModal = () => (
        <Modal
  open={openAnswer}
  onClose={handleCloseAnswer}
  aria-labelledby="modal-modal-title"
  aria-describedby="modal-modal-description"
>
  <Box sx={style2}>
    <Typography id="modal-modal-title" variant="h6" component="h2">
       <strong>Question</strong>: {questionId.question}
    </Typography>
    
    <Typography variant="h6" component="p" style={{textDecoration:'underline'}}>
       <strong >Answers</strong>
    </Typography>
    {answerList.map((answerParam, index)=> {
        return (
            <Paper
                variant="elevation"
                elevation={5}
                style={{marginTop: '4px',
                padding: "4px"
                }}
                variant="h6" component="h2"
            >
                <Typography> {answerParam.answer}</Typography>
            </Paper>
        )
    })}
    <form onSubmit={formSubmitAddQuestion}>
    <TextField
                                    style={{marginTop:"15px"}}
                                    autoComplete="given-name"
                                    name="email"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    autoFocus
                                    variant="outlined"
                                    type="email"
                                    value={emailAnswer}
                                    onChange={(e)=> {setEmailAnswer(e.target.value)}}
                                />
    <TextField
          style={{marginTop:"15px"}}
          id="outlined-multiline-static"
          label="Enter answer"
          multiline
          rows={4}
          fullWidth
          variant="outlined"
          value={suggestion}
          onChange={(e)=>{setSuggesstion(e.target.value)}}
          required
        />
        <Box style={{marginTop:"10px"}}>
            <Button variant="contained" color="primary" type="submit">Submit</Button>
        </Box>
    </form>
  </Box>
</Modal>
    )

    const handleRowClick = (data) => {
        setQuetionId(data);
        console.log(data)
        const fetchMethod ={
            method: "POST",
            body: JSON.stringify({
                question_id: data.question_id
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }
        fetch(`http://localhost:8080/get_answers/`,fetchMethod).then((data)=>data.json()).then((data)=>{
            console.log(data);
            setAnswer(true)
            setAnswerList([...data])
        })
        console.log(data,'[]')
    }


    const handleSubmit = (e) => {
        e.preventDefault();
        const fetchMethod ={
            method: "POST",
            body: JSON.stringify({
                question: email,
                raised_by: password,
                upvotes: 2
            }),
             // Adding headers to the request
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }

        // fetch("", fetchMethod).then(()=> {

        // })
        setQuestion([...questionList,{
            question:email,
            technology: password
        }])
        setOpen(false)
        fetch("http://localhost:8080/add_question/", fetchMethod).then((data)=> {
            // setOpen(false)
        }).catch(()=> {
            // history.push("/login")
        })
    }
    return (<>
        <Box   sx={{
            mt: 6,
            ml: 4
        }}>
        <Button
         variant="contained"
         color="primary"
         sx={{
            mt: 6,
        }}
        onClick={()=>{setOpen(true)}}
         >Add Issues</Button>
        </Box>
        <Typography 
            style={{textAlign:"center"}}
            sx={{
                mt: 6,
                ml: 6
            }}
            variant="h5">Query List</Typography>

            <TableContainer component={Paper} 
            style={{width: "40%", left: 0, right: 0, margin:"auto"}}
            sx={{ maxWidth: 650 ,mt: 2}}>
            <Table  sx={{
    "& .MuiTableRow-root:hover": {
      backgroundColor: "primary.light"
    }
  }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell  align="left" style={{fontSize: "18px"}}>S.no</TableCell>
            <TableCell align="center" style={{fontSize: "18px"}}>Query</TableCell>
            <TableCell align="center" style={{fontSize: "18px"}}>View/Add </TableCell>
        </TableRow>
        </TableHead>
        <TableBody>
            {questionList.map((param,index)=>{
                return (
                    <TableRow
                     key={index}
                    sx={{ '&:last-child td, &:last-child th': { border: 1 } }}
                    style={{ cursor: 'pointer'}}
                  >
                    <TableCell  align="left" style={{fontSize: "18px"}} component="th" scope="row">
                      {index+1}
                    </TableCell>
                    <TableCell align="left" style={{fontSize: "18px"}}>{param.question}</TableCell>
                    <TableCell align="center" style={{fontSize: "18px"}}>
                        <Button onClick={()=>{handleRowClick(param)}} 
                         variant="contained" color="primary">View/Add Answer</Button>
                    </TableCell>
                  </TableRow>
                )
            })}
        </TableBody>
        </Table>
            </TableContainer>
         <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
        <Box sx={style}>
    <Typography id="modal-modal-title" variant="h6" component="h2">
        Add Query
    </Typography>
    <Box component="form"  onSubmit={handleSubmit} sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    autoComplete="given-name"
                                    name="question"
                                    required
                                    fullWidth
                                    id="question"
                                    label="query"
                                    autoFocus
                                    variant="outlined"
                                    type="text"
                                    onChange={(e)=> {setEmail(e.target.value)}}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="email"
                                    label="Email"
                                    type="email"
                                    id="email"
                                    autoComplete="new-password"
                                    sx={{mt:5}}
                                    variant="outlined"
                                    onChange={(e)=>{setPassword(e.target.value)}}
                                />
                            </Grid>
                        </Grid>
                        <Box  sx={{ mt: 5, mb:1, width: "100%"}}>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                             color="primary"

                        >
                            Submit
                        </Button>
                        </Box>
                    </Box>
        </Box>
        </Modal>
        {answerModal()}
            </>)
}


export const Home = ({ login}) => {
    const history = useHistory(); 
    useEffect(()=> {
        let status = JSON.parse(localStorage.getItem('status'))
        if(!status){
            history.push('/')
        }
        else{
        }
    },[])
    return (
        <>
        <ResponsiveDrawer/>

       
        <Route exact   path={`/home`} component={FirstComponent}/>
        <Route exact  path={`/home/createUser`} component={SignUp} />

        </>
    );
}
