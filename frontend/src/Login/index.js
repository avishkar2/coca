import React, { useEffect, useState} from 'react';
import {
    Box,
    Button,
    TextField,
    Checkbox,
    Link,
    Grid,
    Typography,
    Container,
    FormControlLabel,
    ThemeProvider,
    Paper,
    Divider
} from "@material-ui/core"
import { useHistory  } from "react-router-dom";

export default function Login({ setLogin, login}) {
    useEffect(()=> {
        let status = JSON.parse(localStorage.getItem('status'))
        if(status){
            history.push('/home')
        }
    },[])
    const history = useHistory();
    const handleSubmit = (event) => {
        event.preventDefault();
        const fetchMethod ={
            method: "POST",
            body: JSON.stringify({
                email: email,
                password: password
            }),
             // Adding headers to the request
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }
        setEmail('');
        setPassword('')
        fetch("http://localhost:8080/login/", fetchMethod).then((data)=> {
            console.log(data,'][Success')
            if(data.statusText == "Internal Server Error"){
                alert('User doesnot exist')
              
            }
            else{
                localStorage.setItem('status',true);
                history.push('/home')
            }
        })

    };

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('')

    return (
            <Container component="main" maxWidth="xs">
                <Paper variant="elevation" elevation={3}>  
                 <Box
                    sx={{
                        mt: 6,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        padding: 12,
                        boxSizing: "contentBox"
                    }}
                >
                   <Typography component="h1" variant="h5" style={{ fontWeight: 600}}>
                        Log In
                    </Typography>
                    <Box component="form"  onSubmit={handleSubmit} sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    autoComplete="given-name"
                                    name="email"
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    autoFocus
                                    variant="outlined"
                                    type="email"
                                    value={email}
                                    onChange={(e)=> {setEmail(e.target.value)}}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="new-password"
                                    sx={{mt:5}}
                                    variant="outlined"
                                    value={password}
                                    onChange={(e)=>{setPassword(e.target.value)}}
                                />
                            </Grid>
                        </Grid>
                        <Box  sx={{ mt: 5, mb:1, width: "100%"}}>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                             color="primary"

                        >
                            Submit
                        </Button>
                        </Box>
                    </Box>
                </Box>
                </Paper>
                </Container>
    );
}