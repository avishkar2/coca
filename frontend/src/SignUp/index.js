import {
    Box,
    Container,
    FormControl,
    Radio,
    FormControlLabel,
    FormLabel,
    Button,
    Grid, 
    Paper, RadioGroup, TextField, Typography
} from "@material-ui/core";
import { useHistory  } from "react-router-dom";
import React, {useState} from 'react'

const SignUp = () => {
    const history = useHistory();
    const [phoneNo, setPhoneNo] = useState('');
    
    const handleSubmit = (event) => {
        event.preventDefault();
        const fetchMethod ={
            method: "POST",
            body: JSON.stringify({
                email: phoneNo
            }),
             // Adding headers to the request
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        }
        fetch("http://localhost:8080/register/", fetchMethod).then((data)=> {
            if(data.status === 400){
                alert('User already registered')
            }
            else{
                history.push("/")
            }

        }).catch((error)=> {
            console.log(error)
            // history.push("/login")
        })
     }

    return (
        <Container component="div" maxWidth="xs">
            <Paper variant="elevation" elevation={6}>
                <Box
                    sx={{
                        mt: 6,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        padding: 12,
                    }}
                >
                    <Typography component="h1" variant="h5" style={{ fontWeight: 600 }}>
                        Add New User
                    </Typography>
                    <Box component="form" onSubmit={handleSubmit} sx={{ mt: 3 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="email"
                                    label="Email"
                                    type="email"
                                    id="email"
                                    variant="outlined"
                                    autoComplete="email"
                                    onChange={(e)=> { setPhoneNo(e.target.value)}}
                                    sx={{ mt: 5 }}
                                    value={phoneNo}
                                />
                            </Grid>
                        </Grid>
                        <Box sx={{ mt: 2, mb:1}}>
                            <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                             color="primary"
                            >
                            Add
                        </Button>
                        </Box> 

                    </Box>
                </Box>
            </Paper>
        </Container>
    )
}
export default SignUp;