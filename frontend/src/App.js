import { createTheme, ThemeProvider, CssBaseline } from "@material-ui/core";
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Login from "./Login";
import { Home } from "./Home"
import React, { useEffect, useState} from 'react';
import { useHistory  } from "react-router-dom";
const theme = createTheme({
  typography: {
    fontFamily: 'Raleway, Arial',
  },
  palette: {
    primary: {
      main: '#6613e1',
    },
    secondary: {
      main: '#edf2ff',
    },
  },
});
const App = () => {
  const [login, setLogin] = useState(false);
  const history = useHistory(); 

  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <Switch>
            <Route exact path="/" >
              <Login setLogin={setLogin} login={login}/>
            </Route>
            <Route  path="/home" setLogin={setLogin} login={login}>
              <Home />
            </Route>

          </Switch>
        </BrowserRouter>
      </ThemeProvider>
    </>
  );
}
export default App;
